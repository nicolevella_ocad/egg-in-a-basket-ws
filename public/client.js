
let socket = io();
let playercount = 0;
let ringcol = 0;

// values that change based on socket data
let ampOUT, ampIN, rotyOUT, rotxOUT, eggMoveX, eggMoveY, zoff = 0.0;

// value that control the basket loops/noise field
let rotateSpeed = 1;
let ringGap = 0;
let noiseSpeed = 0.75;

// data to broadcast/receive
let dataToSend = {
  micVol: "",
  rotX: "",
  rotY: ""
};

// when someone connects, log it to the console
socket.on("connect", () => {
  console.log("connected to server");
});

// when someone disconnects, log it to the console
socket.on("disconnect", () => {
  console.log("disconnected from server");
});

// functions that receive data from server.js
socket.on("playerDataSend", (e) => {
  ampIN = e.micVol;

  avgX = (e.rotX + rotxOUT) / 2;
  avgY = (e.rotY + rotyOUT) / 2;

  eggMoveX = map(avgX, -180, 180, -(height * 2), height * 2);
  eggMoveY = map(avgY, -90, 90, -(width * 2), width * 2);
});

socket.on("playerCount", (e) => {
  playercount = e.data;
});

// setup the canvas
function setup() {
  // make a canvase thats the size of the browser window
  let cnv = createCanvas(windowWidth, windowHeight);

  // set background and gameplay graphics
  bg = loadImage("img/bg.jpg");
  egg = loadImage("img/egg.png");

  // we have to wait for a user initiated event to get mic access
  cnv.mousePressed(userStartAudio);
  input = new p5.AudioIn();
  input.start();

  // set the framerate, which effectively limits the broadcasts of our socket
  frameRate(30);

  // black stroke and no fill on shapes we draw
  stroke(100, 80, 20);
  noFill();

  // we add a button to the canvase to grant access 
  // to device motion data. especially important to do in iOS 12+
  if (
    typeof DeviceOrientationEvent !== "undefined" &&
    typeof DeviceOrientationEvent.requestPermission === "function"
  ) {
    button = createButton("click to allow access to sensors");
    button.style("font-size", "24px");
    button.center();
    button.mousePressed(requestAccess);
  } else {
    // do nothing, its not a mobile device
  }

}

function draw() {
  // set the background image
  background(bg);

  // only send data from device if its the first or second to connect
  // if (playercount > 2) {
  // set the values of date to send OUT
  // we will use variable names here just to make our code cleaner
  ampOUT = input.getLevel();
  rotxOUT = floor(rotationX);
  rotyOUT = floor(rotationY);
  dataToSend.micVol = ampOUT;
  dataToSend.rotX = rotxOUT;
  dataToSend.rotY = rotyOUT;

  // send the data on each draw loop, as often as the framerate we set above
  socket.emit("playerDataSend", dataToSend);

  // if theres data coming in for a remote mic
  // draw a ring for it
  if (typeof ampIN === 'undefined') {
    ampIN = 0;
  }

  // make the ring for the local player
  makeNestRing(ampOUT);

  // make the ring for the remote player
  makeNestRing(ampIN);

  // draw and move the egg based on socket broadcast data
  push();
  translate((width / 2) - (egg.width / 6), (height / 2) - (height / 6));
  if (typeof eggMoveY === "undefined" || typeof eggMoveX === "undefined") {
    image(egg, 0, 0, egg.width / 3, egg.height / 3);
  } else {
    image(egg, eggMoveY, eggMoveX, egg.width / 3, egg.height / 3);
    // ringcol = map(eggMoveX, -(height * 2), height * 2, 0, 255);
  }
  pop();
}

// event trigger to allow motion data access
// is granted when button is pushed
function requestAccess() {
  DeviceOrientationEvent.requestPermission()
    .then((response) => {
      if (response == "granted") {
        permissionGranted = true;
      }
    })
    .catch(console.error);
  button.remove();
}

// make the audio reactive basket loops
function makeNestRing(noiseLevel) {

  // push the current matrix so we can go back to the draw original position
  push();
  // stroke(ringcol);
  // shift the draw location to the center of the canvas
  translate(width / 2, height / 2);

  // rotate each ring a bit to better distinguish them
  // rotate(radians(ringGap));

  // the "speed" of the wobble is controlled by how fast we traverse through a set of 3D perlin noise
  zoff += noiseSpeed * noiseLevel;

  // begin loop shape
  beginShape();

  // draw a circle
  for (let a = 0; a < TWO_PI; a += radians(1)) {

    // by offsetting the x and y value of each vertex using perlin noise
    // we get a wobbly circle that can start and end on the same point
    let xoff = map(cos(a), -1, 1, 0, noiseLevel * 50);
    let yoff = map(sin(a), -1, 1, 0, noiseLevel * 50);

    // here we make the noise and control the ring radius/width
    let r = noise(xoff, yoff, zoff) * 100 + (width / 3);

    // draw the point
    let x = r * cos(a);
    let y = r * sin(a);
    vertex(x, y);
  }

  // close the loop at the beginning
  endShape(CLOSE);

  // pop back into the original matrix
  pop();
}