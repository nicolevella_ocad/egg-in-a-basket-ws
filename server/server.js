// dependencies
const path = require("path");
const fs = require("fs");
const https = require("https");
const http = require("http");
const express = require("express");
const socketIO = require("socket.io");

// set path to client side files
const publicPath = path.join(__dirname, "/../public");

// set keys for local development on https
// when we push to heroku, we will not need these
var privateKey = fs.readFileSync('key.pem', 'utf8');
var certificate = fs.readFileSync('cert.pem', 'utf8');
var credentials = { key: privateKey, cert: certificate };

// lets use espress to make things easier
let app = express();

// serve files from pubic dir set above
app.use(express.static(publicPath));

// start server
// let httpsServer = https.createServer(credentials, app);
let httpServer = http.createServer(app);

// allow our server to access the socket.io lib
//let io = socketIO(httpsServer);
let io = socketIO(httpServer);

// listen on https/http port
// httpsServer.listen(8443);
httpServer.listen(process.env.PORT || 3000, function () {
  console.log("SERVER STARTED PORT: 3000");
});

let playercount = 0;

// when someone connects, log it to the console using a callback function
io.on('connection', (socket) => {
  playercount++;
  console.log('num players : ' + playercount);
  socket.emit('playerCount', { 'data': playercount })

  // when someone disconnects, log it to the console using a callback function
  socket.on('disconnect', () => {
    playercount--;
    console.log('num players : ' + playercount);
    socket.emit('playerCount', { 'data': playercount })
  });

  // event listener for playerDataSend function in client.js
  socket.on('playerDataSend', (e) => {
    // console.log(e)

    // we want to take the data received and then broadcast it to everyone except the sender
    // so we use socket.broadcast.emit
    socket.broadcast.emit('playerDataSend', e);
  });

})
